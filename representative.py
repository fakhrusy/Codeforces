n, m, a, b = map(int,input().split())
if (n%m) == 0:
    print(0)
else:
    import math
    top = math.ceil(n/m)
    down = math.floor(n/m)
    print(min((m*top-n)*a,(n-down*m)*b))